<?php

namespace Drupal\entity_reference_edit_link\Plugin\Field\FieldWidget;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\Plugin\Field\FieldWidget\EntityReferenceAutocompleteTagsWidget;
use Drupal\Core\Form\FormStateInterface;

/**
 * Class to alter original entity reference autocomplete tags widget.
 */
class EntityReferenceEditLinkAutocompleteTagsWidget extends EntityReferenceAutocompleteTagsWidget {

  /**
   * {@inheritdoc}
   */
  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state) {
    $element = parent::formElement($items, $delta, $element, $form, $form_state);

    // Removes edit link element for the tags autocomplete type.
    unset($element['_link']);

    return $element;
  }

}
