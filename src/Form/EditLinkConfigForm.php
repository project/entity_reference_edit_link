<?php

namespace Drupal\entity_reference_edit_link\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides configuration form for entity reference edit link.
 */
class EditLinkConfigForm extends ConfigFormBase {

  /**
   * Configuration name for edit link config.
   *
   * @const string
   */
  protected const SETTINGS = 'entity_reference_edit_link.config';

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    $instance = parent::create($container);
    $instance->entityTypeManager = $container->get('entity_type.manager');

    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'entity_reference_edit_link_config_form';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [self::SETTINGS];
  }

  /**
   * {@inheritdoc}
   *
   * @param array $form
   *   A nested array form elements comprising the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config(self::SETTINGS);
    $content_types = [];
    $entities = $this->entityTypeManager->getStorage('node_type')->loadMultiple();
    foreach ($entities as $entity) {
      $content_types[$entity->id()] = $entity->label();
    }
    // Section for configuring content types.
    $form['edit_link_config'] = [
      '#type' => 'details',
      '#title' => $this->t('Configure Content Types'),
      '#description' => $this->t('Select the content type here if you want to add link on node edit page to manage display page of a node.'),
      '#open' => TRUE,
    ];
    $form['edit_link_config']['content_types'] = [
      '#type' => 'checkboxes',
      '#options' => $content_types,
      '#default_value' => $config->get('content_types') ?: [],
    ];
    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);
    $values = $form_state->getValues();
    $this->config(self::SETTINGS)
      ->set('content_types', $values['content_types'])
      ->save();
  }

}
