# Entity Reference Edit Link

The module should be useful for content editors because it allows them to easily navigate to the entity that is used as an entity reference.

Works with the Select2 field widget.
1. Adds an edit link to the entity for the entity reference field.
2. Adds a link to the content type "Manage fields" page on the node edit page (this function needs to be enabled on the configuration page).

The module `1.1.0` version and higher has a configuration page to enable links for the content type "Manage fields" page here `/admin/config/entity-reference-edit-link`
## Installation

Install as you would normally install a contributed Drupal module. For further information, see [Installing Drupal Modules](https://www.drupal.org/docs/extending-drupal/installing-drupal-modules).


## Maintainers 

- Kostia Bohach ([_shy](https://www.drupal.org/u/_shy))